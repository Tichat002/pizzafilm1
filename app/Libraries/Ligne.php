<?php
namespace App\Libraries;
class Ligne 
{

    public string $produit;
    public int $quantite;



	public function __construct($produit, $quantite)
	{
		$this->produit = $produit;
		$this->quantite = $quantite;
	}	


    public function getProduit()
    {
        return $this->produit;
    }
    
    
    public function getQuantite()
    {
        return $this->quantite;
    }

}

?>