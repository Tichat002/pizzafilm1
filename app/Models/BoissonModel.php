<?php
namespace App\Models;
use CodeIgniter\Model;

class BoissonModel extends Model{
    protected $table = 'Boisson';
    protected $primaryKey = 'BoissonID';
    protected $allowedFields= [
            'BoissonName'
        ];



        
    public function getBoisson()
    {
        $this->select('BoissonName', 'BoissonID');
        $query=$this->get();
        return ($query->getResult('array'));
    }

}

