<?php
namespace App\Models;
use CodeIgniter\Model;

class PizzaModel extends Model{
    protected $table = 'Pizza';
    protected $primaryKey = 'PizzaID';
    protected $allowedFields= [
            'PizzaName'
        ];



    public function getPizza()
    {
        $this->select('PizzaName', 'PizzaID');
        $this->from('Pizza');
        $query=$this->get();
        return ($query->getResult('array'));
    }    
}
