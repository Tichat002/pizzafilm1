<?php
namespace App\Models;
use CodeIgniter\Model;

class CommanderModel extends Model{
    protected $table = 'Pizza';
    protected $primaryKey = 'PizzaID';
    protected $allowedFields= [
            'PizzaName'
        ];



        
    public function getPizza()
    {
        $this->select('PizzaName', 'PizzaID');
        $query=$this->get();
        return ($query->getResult('array'));
    }

}

