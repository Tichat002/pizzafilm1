<?php
namespace App\Models;
use CodeIgniter\Model;

class CategorieModel extends Model{
    protected $table = 'categorie';
    protected $primaryKey = 'CategorieID';
    protected $allowedFields= [
            'CategorieName'
        ];



    public function getCategorie()
    {
        $this->select('CategorieName', 'CategorieID');
        $this->from('categorie');
        $query=$this->get();
        return ($query->getResult('array'));
    }


}

