<?php
namespace App\Models;
use CodeIgniter\Model;

class BoissonModel extends Model{
    protected $table = 'Boisson';
    protected $primaryKey = 'BoissonID';
    protected $allowedFields= [
            'BoissonName'
        ];



    public function getBoissonById(int $serID)
    {
        $this->select('BoissonName', 'BoissonID');
        $this->from('serie');
        $this->where('BoissonID', $serID);
        $query=$this->get();
        return ($query->getResult('array'));
    }
}

