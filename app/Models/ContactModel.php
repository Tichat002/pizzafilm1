<?php
namespace App\Models;
use CodeIgniter\Model;

class ContactModel extends Model{
    protected $table = 'Contact';
    protected $primaryKey = 'ContactID';
    protected $allowedFields= [
            'name',
            'fname',
            'email',
            'message'
        ];



        
}