<?php
namespace App\Models;
use CodeIgniter\Model;

class FilmModel extends Model{
    protected $table = 'Film';
    protected $primaryKey = 'FilmID';
    protected $allowedFields= [
            'FilmTitre',
            'CategorieID',
            'PizzaID',
            'SerieID'
        ];
}

