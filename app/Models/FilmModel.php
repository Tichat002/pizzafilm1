<?php
namespace App\Models;
use CodeIgniter\Model;

class FilmModel extends Model{
    protected $table = 'Film';
    protected $primaryKey = 'FilmID';
    protected $allowedFields= [
            'FilmName'
        ];



    public function getFilm()
    {
        $this->select('FilmName', 'FilmID');
        $this->from('Film');
        $query=$this->get();
        return ($query->getResult('array'));
    }    
}

