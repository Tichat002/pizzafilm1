<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PizzaFilm</title>
	<meta name="description" content="Regarder vos film en mangeant une pizza!">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" type="image/png" href="/favicon.ico"/>
    <link rel="stylesheet" href="../../public/assets/css/style.css">
</head>
<body>

<!-- HEADER: MENU + HEROE SECTION -->
<header>

	<?php 
    require("header.php");
    ?>

<div class="heroe">

<h1>PizzaFilm</h1>

<h2>Quoi de mieux qu'une pizza devant un bon film?</h2>

</div>
</header>

<!-- CONTENT -->

<section>

<a href="<?php echo base_url(); ?>/IndexController/Commander"> <input type="button" value="Commander" class="button" id="TESTbutton"> </a>

</section>

<div class="further">

	<section>

		<h2>
			<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'><rect x='32' y='96' width='64' height='368' rx='16' ry='16' style='fill:none;stroke:#000;stroke-linejoin:round;stroke-width:32px'/><line x1='112' y1='224' x2='240' y2='224' style='fill:none;stroke:#000;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/><line x1='112' y1='400' x2='240' y2='400' style='fill:none;stroke:#000;stroke-linecap:round;stroke-linejoin:round;stroke-width:32px'/><rect x='112' y='160' width='128' height='304' rx='16' ry='16' style='fill:none;stroke:#000;stroke-linejoin:round;stroke-width:32px'/><rect x='256' y='48' width='96' height='416' rx='16' ry='16' style='fill:none;stroke:#000;stroke-linejoin:round;stroke-width:32px'/><path d='M422.46,96.11l-40.4,4.25c-11.12,1.17-19.18,11.57-17.93,23.1l34.92,321.59c1.26,11.53,11.37,20,22.49,18.84l40.4-4.25c11.12-1.17,19.18-11.57,17.93-23.1L445,115C443.69,103.42,433.58,94.94,422.46,96.11Z' style='fill:none;stroke:#000;stroke-linejoin:round;stroke-width:32px'/></svg>
			Apprendre
		</h2>

		<p>apprenez en plus sur notre entreprise en cliquant sur <a href="">information</a> !</p>


	</section>

</div>

<!-- FOOTER: DEBUG INFO + COPYRIGHTS -->

<footer>

	<div class="copyrights">

		<p>&copy; <?= date('Y') ?> PizzaFilm</p>

	</div>

</footer>

<!-- SCRIPTS -->

<script>
	function toggleMenu() {
		var menuItems = document.getElementsByClassName('menu-item');
		for (var i = 0; i < menuItems.length; i++) {
			var menuItem = menuItems[i];
			menuItem.classList.toggle("hidden");
		}
	}
</script>

<!-- -->

</body>
</html>
