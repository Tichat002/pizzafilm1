<!doctype html>
<html lang="en">
    
<head>    
    
    <meta charset="utf-8">    
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">    
    
    <title>Inscription</title></head>
<body>    
    <div class="container mt-5">        
        <div class="row justify-content-md-center">            
            <div class="col-5">                
                <h2>Inscription</h2>
                <?php if(isset($validation)):?>                
                <div class="alert alert-warning">
                    <?=$validation->listErrors() ?>                
                </div>
                <?php endif;?>                
                <form action="<?php echo base_url(); ?>/SignupController/store" method="post">    
                <input type="hidden" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>">                
                <div class="form-group mb-3">                        
                    <input type="text" name="UserName" placeholder="UserName" value="<?=set_value('UserName') ?>" class="form-control" >                    
                </div>                    
                <div class="form-group mb-3">                        
                    <input type="UserMail" name="UserMail" placeholder="UserMail" value="<?= set_value('UserMail') ?>" class="form-control" >
                </div>                    
                <div class="form-group mb-3">                        
                    <input type="UserPassword" name="UserPassword" placeholder="UserPassword" class="form-control" >                    
                </div>                    
                <div class="form-group mb-3">                        
                    <input type="password" name="confirmpassword" placeholder="Confirm Password" class="form-control" >                    
                </div>                    
                <div class="d-grid">                        
                    <button type="submit" class="btn btn-dark">S'inscrire</button>                    
                </div>                
            </form>            
        </div>        
    </div>    
</div>
</body>
</html>