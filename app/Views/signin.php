<!doctype html>
<html lang="en">  
    <head>    
        <meta charset="utf-8">    
        <meta name="viewport" content="width=device-width, initial-scale=1">    
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">    
        <title>Codeigniter Login with Email/Password Example</title>  
    </head>  
    <body>   
    <a href="index"> <input type="button" value="Home" class="btn btn-primary mb-2"> </a>
        <div class="container">        
            <div class="row justify-content-md-center">            
                <div class="col-5">                
                    <h2>Connexion</h2><?php if(session()->getFlashdata('msg')):?>                   
                         <div class="alert alert-warning"><?= session()->getFlashdata('msg') ?>                    
                        </div><?php endif;?>                
                        <form action="<?php echo base_url(); ?>/SigninController/loginAuth" method="post">     
                        <input type="hidden" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>">               
                        <div class="form-group mb-3">                        
                            <input type="UserMail" name="UserMail" placeholder="UserMail" value="<?= set_value('UserMail') ?>" class="form-control" >         
                        </div>                    
                        <div class="form-group mb-3">                        
                            <input type="UserPassword" name="UserPassword" placeholder="UserPassword" class="form-control" >                    
                        </div>                    
                        <div class="d-grid">                         
                            <button type="submit" class="btn btn-success">Connexion</button>                    
                        </div>                     
                    </form>            
                </div>        
            </div>    
        </div>  
    </body>
</html>