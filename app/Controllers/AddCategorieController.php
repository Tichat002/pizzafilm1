<?php
namespace App\Controllers;
  use CodeIgniter\Controller;
  use App\Models\CategorieModel;
  class AddCategorieController extends Controller
  {public function index()    
    {        
        helper(['form']);
        $data= [];
        echo view('addCategorie', $data); 
    }
    public function store()    
    {        
        helper(['form']);
        $rules= ['CategorieName'=>'required|min_length[2]|max_length[150]'];
        if($this->validate($rules)){
            $categorieModel= new CategorieModel();
            $data= [
                'CategorieName'=>$this->request->getVar('CategorieName'),

                   ];
                   $categorieModel->save($data);
                   return redirect()->to('/addCategorie'); 
                    }else{
                        $data['validation'] =$this->validator;
                        echo view('addCategorie', $data);        
                    }
                }
            }
