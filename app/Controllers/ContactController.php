<?php
namespace App\Controllers;
  use CodeIgniter\Controller;
  use App\Models\ContactModel;
  class ContactController extends Controller
  {public function index()    
    {        
        helper(['form']);
        $data= [];
        echo view('contact', $data); 
    }
    public function Message()    
    {        
        helper(['form']);
        $rules= 
        [
          'name'=>'required|min_length[2]|max_length[150]',
          'email'=>'required|min_length[2]|max_length[300]',
          'fname'=>'required|min_length[2]|max_length[150]',
          'message'=>'required|min_length[2]|max_length[150]'

        ];
        if($this->validate($rules)){
            $contactModel= new ContactModel();
            $data= [
              'name'=>$this->request->getVar('name'),
              'email'=>$this->request->getVar('email'),
              'fname'=>$this->request->getVar('fname'),
              'message'=>$this->request->getVar('message'),

                   ];
                   $contactModel->save($data);
                   return redirect()->to('/contact'); 
                    }else{
                        $data['validation'] =$this->validator;
                        echo view('contact', $data);        
                    }
                }
            }
