<?php
namespace App\Controllers;
  use CodeIgniter\Controller;
  use App\Models\PizzaModel;
  use App\Models\FilmModel;
  use App\Models\SerieModel;
  use App\Models\CategorieModel;
  class WebServiceController extends Controller
  {
    public function index()
    {
        helper(['form']);
        $data= [];
        echo view();
    }


    public function getCategories()
    {
        //j'instancie le modele
        $cm = new CategorieModel();
        //je recupere les donnees a partir du modele
        $lesCategorie = $cm->getCategorie();
        //je genere la vue (il etait prévu d'utiliser une vue a part -JSonCategorieView - pour aller plus vite, on affiche le json directement)
        echo json_encode($lesCategorie);
    }

    public function getFilmByCategorieID($id)
    {
        $LbCm = new FilmModel();
        $LesFilmParCategorie = $LbCm->getFilmByCategorieID($id);
        echo json_encode($LesFilmParCategorie);
    }

    public function getFilmBySerieID($id)
    {
        $LbSm = new FilmModel();
        $LesFilmParSerie = $LbSm->getFilmBySerieID($id);
        echo json_encode($LesFilmParSerie);
    }

    public function getSeriebyID($id)
    {
        $sm = new SerieModel();
        $LesSerie = $sm->getSeriebyID($id);
        echo json_encode($LesSerie);
    }
};