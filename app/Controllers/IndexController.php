<?php
namespace App\Controllers;
  use CodeIgniter\Controller;
  class IndexController extends Controller
  {public function index()    
    {        
        helper(['form']);
        $data= [];
        echo view('index', $data); 
    }
    public function commander()
    {
      return redirect()->to('/commander');
    }
  }
