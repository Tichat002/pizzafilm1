<?php
namespace App\Controllers;
  use CodeIgniter\Controller;
  use App\Models\FilmModel;
  class AddFilmController extends Controller
  {public function index()
    {
        helper(['form']);
        $data= [];
        echo view('addFilm', $data);
    }
    public function store()
    {
        helper(['form']);
        $rules= ['FilmName'=>'required|min_length[2]|max_length[150]'];
        if($this->validate($rules)){
            $FilmModel= new FilmModel();
            $data= [
                'FilmName'=>$this->request->getVar('FilmName'),

                   ];
                   $FilmModel->save($data);
                   return redirect()->to('/addFilm'); 
                    }else{
                        $data['validation'] =$this->validator;
                        echo view('addFilm', $data);
                    }
                }
            }
