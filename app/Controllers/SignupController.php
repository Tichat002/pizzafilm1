<?php
namespace App\Controllers;
  use CodeIgniter\Controller;
  use App\Models\UserModel;
  class SignupController extends Controller
  {public function index()    
    {        
        helper(['form']);
        $data= [];
        echo view('signup', $data);
    }
    public function store()    
    {        
        helper(['form']);
        $rules= [
            'UserName'=>'required|min_length[2]|max_length[150]',
                 'UserMail'=>'required|min_length[4]|max_length[150]|valid_email|is_unique[user.UserMail]',
                 'UserPassword'=>'required|min_length[4]|max_length[150]',
                 'confirmpassword'=>'matches[UserPassword]'
                ];
        if($this->validate($rules)){
            $userModel=new UserModel();
            $data= ['UserName'=>$this->request->getVar('UserName'),
                'UserMail'=>$this->request->getVar('UserMail'),
                'UserPassword'=>password_hash($this->request->getVar('UserPassword'), PASSWORD_DEFAULT) ];
                   $userModel->save($data);
                   return redirect()->to('/signin');
                    }else{
                        $data['validation'] =$this->validator;
                        echo view('signup', $data);        
                    }
                }
            }