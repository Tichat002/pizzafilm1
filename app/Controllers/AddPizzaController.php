<?php
namespace App\Controllers;
  use CodeIgniter\Controller;
  use App\Models\PizzaModel;
  class AddPizzaController extends Controller
  {public function index()
    {
        helper(['form']);
        $data= [];
        echo view('addPizza', $data);
    }
    public function store()
    {
        helper(['form']);
        $rules= ['PizzaName'=>'required|min_length[2]|max_length[150]'];
        if($this->validate($rules)){
            $PizzaModel= new PizzaModel();
            $data= [
                'PizzaName'=>$this->request->getVar('PizzaName'),

                   ];
                   $PizzaModel->save($data);
                   return redirect()->to('/addPizza'); 
                    }else{
                        $data['validation'] =$this->validator;
                        echo view('addPizza', $data);
                    }
                }
            }
