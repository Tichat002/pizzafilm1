<?php

namespace App\Controllers;
  use CodeIgniter\Controller;
  use App\Models\CommanderModel;
  use App\Libraries\Ligne;
  
  
  
 
  class CommanderController extends Controller
  {
      
    public function index()    
    {        
        helper(['form']);
        $data = [];
        $bm=new CommanderModel();
        $data["pizzas"]=$bm->getPizza();
        echo view('commander', $data);
    }
    
    public function button()
    { $session = session();
      $pizza=$this->request->getVar('pizza');
      $quantite=1; //a changer

      $lignepizza = new Ligne($pizza,$quantite);
      $caddie1=$session->get('Caddie');
      $caddie1->add($lignepizza->getProduit(), $lignepizza->getQuantite()); 

      return redirect()->to('/boisson');
    }
  }