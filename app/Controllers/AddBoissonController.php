<?php
namespace App\Controllers;
  use CodeIgniter\Controller;
  use App\Models\BoissonModel;
  class AddBoissonController extends Controller
  {public function index()
    {
        helper(['form']);
        $data= [];
        echo view('addBoisson', $data);
    }
    public function store()
    {
        helper(['form']);
        $rules= ['BoissonName'=>'min_length[2]|max_length[150]'];
        if($this->validate($rules)){
            $boissonModel= new BoissonModel();
            $data= [
                'BoissonName'=>$this->request->getVar('BoissonName'),

                   ];
                   $boissonModel->save($data);
                   return redirect()->to('/addBoisson');
                    }else{
                        $data['validation'] =$this->validator;
                        echo view('addBoisson', $data);
                    }
                }
            }
