<?php
namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\UserModel;
class SigninController extends Controller
{
    public function index()
    {
        helper(['form']);
        echo view('signin');    
    } public function loginAuth()    
    {
        $session= session();
        $userModel=new UserModel();
        $email=$this->request->getVar('UserMail');
        $password=$this->request->getVar('UserPassword');
        $data=$userModel->where('UserMail', $email)->first();
        if($data){
            $pass=$data['UserPassword'];
            $authenticatePassword=password_verify($password, $pass);
            if($authenticatePassword){
                $ses_data= [
                    'UserID'=>$data['UserID'],
                    'UserName'=>$data['UserName'],
                    'UserMail'=>$data['UserMail'],
                    'isLoggedIn'=>TRUE                
                ];
                $session->set($ses_data);
                return redirect()->to('/profile');            
            }else{
                $session->setFlashdata('msg', 'Password is incorrect.');
                return redirect()->to('/signin');            
            }        
        }else{
            $session->setFlashdata('msg', 'Email does not exist.');
            return redirect()->to('/signin');
        }    
    }
}